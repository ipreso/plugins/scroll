# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
# *****************************************************************************
#  Description : Makefile to build iPreso's plugins
#  Auteur      : Marc Simonetti
# *****************************************************************************

PLUGIN_BIN=scroll.plugin
PLAYER_BIN=iScrollBin

VERSION_PLUGIN=1
CURRENTDIR=$(shell basename `pwd`)

RM=@rm -f
CP=@cp
CC=@gcc

CC_OPT=-L.
CLIBS=-shared
INCLUDEDIR=-I/usr/include/iplayer
LIBDIR=-L/usr/lib/iplayer
LIBS=-liPlayer -ldl $(LIBDIR)
CFLAGS=-Wall -Wunused -g -O -fPIC $(INCLUDEDIR)

# *****************************************************************************
# Operations
%.o: %.c
	@echo " [GCC] $@"
	$(CC) -o $@ -c $< $(CFLAGS)

# *****************************************************************************
# Dependances
iPlugScroll.o:      iPlugScroll.c
iScrollBin.o:       iScrollBin.c

# *****************************************************************************
# Targets
all: $(PLUGIN_BIN) $(PLAYER_BIN)

clean:
	@echo -n "* Cleaning $(CURRENTDIR)... "
	$(RM) *.o $(PLUGIN_BIN) $(PLAYER_BIN)
	@echo "Ok"

install:
	@echo -n "* Installing $(CURRENTDIR)... "
	@mkdir -p $(DESTDIR)/var/lib/iplayer/lib
	@cp $(PLUGIN_BIN) $(DESTDIR)/var/lib/iplayer/lib/
	@cp $(PLAYER_BIN) $(DESTDIR)/var/lib/iplayer/lib/
	@strip --strip-unneeded $(DESTDIR)/var/lib/iplayer/lib/$(PLUGIN_BIN)
	@chmod 0644 $(DESTDIR)/var/lib/iplayer/lib/$(PLUGIN_BIN)
	@mkdir -p $(DESTDIR)/usr/bin
	@ln -sf ../../var/lib/iplayer/lib/$(PLAYER_BIN) $(DESTDIR)/usr/bin/$(PLAYER_BIN)
	@echo "Ok"

# *****************************************************************************
# Edition de liens
$(PLUGIN_BIN): iPlugScroll.o
	@echo " [LNK] $@"
	$(CC) -o $@ $^ $(CC_OPT) $(CLIBS) $(LIBS) -Wl,-soname,libiPlugScroll.so.$(VERSION_PLUGIN)

$(PLAYER_BIN): $(PLAYER_BIN).o
	@echo " [LNK] $@"
	$(CC) -o $@ $^ -lglut -lGLU -lGLC -lGL -lrt
